import time
import jwt
import requests

from .posts import Posts
from .exceptions import (
    UnauthorizedException,
    InvalidCredentialsException,
)


class DemoClient:
    API_URL = 'http://localhost:8000'

    def __init__(self, username, password):
        self._username = username
        self._password = password

        self._token = None

        if not self.authorize():
            raise InvalidCredentialsException

        self.Posts = Posts(self)

    def authorize(self):
        if self._token:
            # We already have a token, let's test if it's still valid
            token = jwt.decode(self._token, verify=False)
            if token['exp'] > time.time():
                return True

            # Oh no! Let's get a new one then
            if self._get_refresh_token():
                # If we got a new one, awesome
                return True
            # If not however, let's authorize again

        return self._get_token()

    def _get_token(self):
        req = self.post('/accounts/auth', {
            'username': self._username,
            'password': self._password,
        }, auth=False)
        data = req.json()

        if req.status_code == 200 and 'token' in data:
            self._token = data['token']
            return True

        if data.status_code == 400:
            raise InvalidCredentialsException

    def _get_refresh_token(self):
        req = self.post('/accounts/auth/refresh', {
            'token': self._token
        }, auth=False)
        data = req.json()

        if req.status_code == 200 and 'token' in data:
            self._token = data['token']
            return True

        if data.status_code == 400:
            # Seems our token has expired and can't be refreshed
            return False

    # request methods
    def get(self, url, auth=True):
        return self.call(url)

    def post(self, url, data=None, auth=True):
        return self.call(url, data, 'post')

    def put(self, url, data=None, auth=True):
        return self.call(url, data, 'put')

    def delete(self, url, data=None, auth=True):
        return self.call(url, data, 'delete')

    def call(self, url, data=None, method='get', auth=True):
        caller = getattr(requests, method)

        # Only send headers if required
        headers = {}
        if auth:
            headers['Authorization'] = 'JWT %s' % self._token

        return caller(self.API_URL + url, data, headers=headers)
