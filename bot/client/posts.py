class Posts:

    def __init__(self, client):
        self._client = client

    def all(self):
        req = self._client.get('/posts/')
        return req.json()

    def by_user(self, user):
        req = self._client.get('/posts/by-user/%s' % user)
        return req.json()

    def new(self, text):
        req = self._client.post('/posts/', {'text': text})
        return req.json()

    def get(self, post):
        req = self._client.get('/posts/%s' % post)
        return req.json()

    def edit(self, post, text):
        req = self._client.put('/posts/%s' % post, {'text': text})
        return req.json()

    def delete(self, post, text):
        req = self._client.delete('/posts/%s' % post)
        return req.json()

    def like(self, post):
        req = self._client.post('/posts/%s/like' % post)
        return req.json()

    def unlike(self, post):
        req = self._client.delete('/posts/%s/like' % post)
        return req.json()
