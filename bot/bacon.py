"""
I hope you like bacon as much as I do.
"""

import requests


def get_some_bacon(paragraphs=3):
    req = requests.get('https://baconipsum.com/api/?type=all-meat&paras=%s' % paragraphs)
    req.raise_for_status()
    return req.json()


def get_a_bacon():
    bacon = None

    for x in iter(int, 1):
        if not bacon:
            bacon = get_some_bacon()
        
        yield bacon.pop()
