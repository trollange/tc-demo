from django.db import models, utils as dbutils
from django.contrib.auth import get_user_model


class Post(models.Model):

    text = models.TextField(blank=False, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(get_user_model())

    def like_count(self):
        return self.like_set.count()

    def like(self, user):
        """
        Attempts to like a post as a :user.
        Returns true if like successful, or false if already liked by user.
        """
        lk = Like(post=self, user=user)
        try:
            lk.save()
            return True, lk
        except dbutils.IntegrityError:
            return False, lk

    def unlike(self, user):
        """
        Attempts to unlike a post as a :user.
        Returns true if unlike successful, or false if not liked by user.
        """
        try:
            lk = Like.objects.get(post=self, user=user)
            lk.delete()
            return True
        except Like.DoesNotExist:
            return False


class Like(models.Model):

    post = models.ForeignKey(Post)
    user = models.ForeignKey(get_user_model())
    liked_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('post', 'user', )
