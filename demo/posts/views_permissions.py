from rest_framework import permissions


class PostPermissions(permissions.BasePermission):
    """
    Post owner can GET, POST, PUT, DELETE
    Staff can GET, DELETE
    Others can only GET
    """

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return True
            
        elif request.method == 'DELETE' and request.user.is_staff:
            return True

        return request.user == obj.created_by
