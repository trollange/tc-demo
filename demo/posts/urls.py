from django.conf.urls import url

from . import views


urlpatterns = [
    # Basic CRUD
    url(r'^$', views.PostListCreate.as_view()),
    url(r'^(?P<pk>[0-9]+)$', views.PostDetail.as_view()),

    # List posts by user
    url(r'^by-user/(?P<user_id>[0-9]+)$', views.PostListByUser.as_view()),

    # Like/unlike
    url(r'^(?P<post_id>[0-9]+)/like$', views.PostLike.as_view()),
]
