from django.conf import urls
from rest_framework.response import Response
from rest_framework.decorators import api_view


@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def handler404(request):
    return Response({'error': 'Resource not found.'}, status=404)


@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def handler500(request):
    return Response({'error': 'Seems that there was an error we didn\'t anticipate.'}, status=500)

__all__ = [
    'handler404',
    'handler500',
]
