from django.db import models
from django.contrib.auth import get_user_model


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    autopopulated = models.BooleanField(default=False)

    linkedin = models.CharField(max_length=50, null=True)
    company = models.CharField(max_length=100, null=True)
