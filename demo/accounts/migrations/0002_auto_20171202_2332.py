# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-02 23:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='company',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='linkedin',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
