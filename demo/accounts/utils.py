from django.conf import settings
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

import requests


def email_is_valid(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False

def verify_email(email):
    """
    Hunter.io pokes mail server and tells us whether the
    email the user gave us is valid or nope.
    """
    # but first, the regex check
    if not email_is_valid(email):
        return {'valid': False, 'message': 'Email address format is invalid.'}

    url = 'https://api.hunter.io/v2/email-verifier?email={email}&api_key={key}'
    url = url.format(email=email, key=settings.HUNTER_IO_API_KEY)

    req = requests.get(url)
    data = req.json()

    if 'errors' in data:
        if data['errors'][0]['id'] == 'wrong_params':
            return {'valid': False, 'message': 'Invalid email address.'}
        # else
        # Invalid API key or quota reached.
        # TODO: Periodically check quota levels, and mail admins when it
        # goes below some threshold (settings value)

    # Possible options (service aims at B2B use cases):
    # - deliverable
    # - risky (returned for free/open webmails mostly)
    # - undeliverable
    if data['data']['result'] == 'undeliverable':
        return {'valid': False, 'message': 'Email address does not exist, '
                                           'or cannot be delivered to.'}
    return {'valid': True}
