from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Profile
from .utils import verify_email
from .tasks import init_profile


class ProfileSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source='user.id')
    username = serializers.ReadOnlyField(source='user.username')
    email = serializers.ReadOnlyField(source='user.email')

    class Meta:
        model = Profile
        fields = ('id', 'username', 'email', 'linkedin', 'company', )

    # def update(self, instance, validated_data):
    #     if 'password' in validated_data:
    #         password = validated_data.pop('password')
    #         instance.user.set_password(password)
    #     return super(ProfileSerializer, self).update(instance, validated_data)

class BasicUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('id', 'username', )


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer used for registration exclusively.
    """

    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'password', 'email', )
        extra_kwargs = {
            'email':    {'required': True},
            'username': {'required': True},
            'password': {'write_only': True},
        }

    def validate_email(self, value):
        if get_user_model().objects.filter(email=value).exists():
            raise serializers.ValidationError('A user with that email already exists.')

        status = verify_email(value)

        if not status['valid']:
            raise serializers.ValidationError(status['message'])
        return value

    def create(self, validated_data):
        user = get_user_model().objects.create_user(**validated_data)

        # Create a user profile as well
        profile = Profile.objects.create(user=user)
        # Queue profile population
        init_profile.apply_async((user.id, ))

        return user
