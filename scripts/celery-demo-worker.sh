#!/usr/bin/sh

# This script should be used for demonstration only
# In production setting, you should run celery via Supervisord
# <https://realpython.com/blog/python/asynchronous-tasks-with-django-and-celery/>

# Get Django's SECRET_KEY into env
# if it's not already present
if [ -z $SECRET_KEY ]; then
    read -p "Django's SECRET_KEY: " SECRET_KEY
fi

# Same for Clearbit's key
if [ -z $CLEARBIT_API_KEY ]; then
    read -p "Clearbit API key: " CLEARBIT_API_KEY
fi

celery -A demo worker -l info
